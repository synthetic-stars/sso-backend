import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly configService: ConfigService,
  ) {}

  async findOne(id: number) {
    const user = await this.usersRepository.findOne(id);
    if (!user) {
      return null;
    }

    return user;
  }

  async findOneByUsername(username: string) {
    const user = await this.usersRepository.findOne({
      where: { username: username },
    });
    if (!user) {
      return null;
    }

    return user;
  }

  async create(createUserDto: CreateUserDto): Promise<[User, string[]]> {
    if (await this.findOneByUsername(createUserDto.username)) {
      throw new BadRequestException(`User already exists.`);
    }
    const hashed_password = await bcrypt.hash(
      createUserDto.password,
      this.configService.get<number>('bcryptSaltRounds'),
    );
    const bakCodes: string[] = User.genBakCodes();
    const bakCodesHashes: string[] = [];
    for (var code of bakCodes) {
      const hash = await bcrypt.hash(
        code,
        this.configService.get<number>('bcryptSaltRounds'),
      );
      bakCodesHashes.push(hash);
    }
    const user = this.usersRepository.create({
      ...createUserDto,
      password: hashed_password,
      backupCodes: bakCodesHashes,
    });
    return [await this.usersRepository.save(user), bakCodes];
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRepository.preload({
      id: +id,
      ...updateUserDto,
    });

    if (!user) {
      throw new NotFoundException(`User not found`);
    }

    return await this.usersRepository.save(user);
  }

  async updatePassword(id: number, password: string) {
    const hashedPassword = await bcrypt.hash(
      password,
      this.configService.get<number>('bcryptSaltRounds'),
    );
    return this.update(id, {password: hashedPassword})
  }

  async remove(id: number) {
    const user = await this.findOne(id);
    return this.usersRepository.remove(user);
  }

  async checkBakCode(user: User, code: string): Promise<string> {
    for (var hash of user.backupCodes) {
      const match = await bcrypt.compare(code, hash);
      if (match) {
        return hash
      }
    }
    return ''
  }
}
