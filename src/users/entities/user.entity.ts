import { ApiHideProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  @Exclude()
  @ApiHideProperty()
  password: string;

  @Column({ type: 'json', nullable: true })
  @Exclude()
  @ApiHideProperty()
  backupCodes: string[];

  @Column({ nullable: true, default: null, unique: true })
  @Exclude()
  @ApiHideProperty()
  nakamaCustomId: string;

  static genBakCodes(): string[] {
    const len: number = 8;
    const chars: string =
      '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const codes: string[] = [];
    for (var i = 0; i < 6; i++) {
      var result: string = '';
      for (var k = len; k > 0; --k) {
        result += chars[Math.round(Math.random() * (chars.length - 1))];
      }
      codes.push(result);
    }
    return codes;
  }

}
