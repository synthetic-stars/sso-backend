import { IsString, Length, Matches } from "class-validator";
import { Match } from "src/common/decorators/match.decorator";

export class CreateUserDto {
  @IsString()
  @Length(3, 20)
  @Matches(/^[\w\.\-]{3,20}$/gm)
  readonly username: string;
  @IsString()
  readonly password: string;
  @IsString()
  @Match('password')
  readonly confirmPassword: string;
}
