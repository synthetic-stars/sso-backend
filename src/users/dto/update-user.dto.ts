import { PartialType } from '@nestjs/mapped-types';
import { IsArray, IsString, IsUUID, ValidateNested } from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  nakamaRefreshJwt?: string;
  @IsUUID()
  nakamaCustomId?: string;
  @IsArray()
  @IsString({ each: true })
  backupCodes?: string[];
}
