import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class RefreshJwtAuthGuard extends AuthGuard('refreshJwt') {}

@Injectable()
export class JwtAuthGuard extends AuthGuard('accessJwt') {}
