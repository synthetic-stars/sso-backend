import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { lastValueFrom } from 'rxjs';
import { SupportedServices } from 'src/common/enum/supported-services.enum';
import { RefreshTokensCacheService } from 'src/refresh-tokens-cache/refresh-tokens-cache.service';
import { User } from 'src/users/entities/user.entity';
import { UsersService } from 'src/users/users.service';
import { v4 as uuidv4 } from 'uuid';
import { ChangePasswordDto } from './dto/change-password.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly httpService: HttpService,
    private readonly refreshTokensCacheService: RefreshTokensCacheService,
  ) {}
  private readonly logger = new Logger(AuthService.name);

  async validateUser(username: string, password: string) {
    const user = await this.usersService.findOneByUsername(username);
    if (!user) {
      return null;
    }
    const match = await bcrypt.compare(password, user.password);
    if (match) {
      return user;
    }
    return null;
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto) {
    const user = await this.usersService.findOneByUsername(
      resetPasswordDto.username,
    );
    const match = await this.usersService.checkBakCode(
      user,
      resetPasswordDto.bakCode,
    );
    if (match == '') {
      throw new BadRequestException('Invalid backup code');
    }
    this.usersService.updatePassword(user.id, resetPasswordDto.password);
    const remainingCodes = user.backupCodes.filter((val) => {
      return val != match;
    });
    this.updateNakamaToken(user);
    return this.usersService.update(user.id, { backupCodes: remainingCodes });
  }

  async changePassword(id: number, changePasswordDto: ChangePasswordDto) {
    const user = await this.usersService.findOne(id);
    if (!user) {
      throw new NotFoundException();
    }

    const match = await bcrypt.compare(
      changePasswordDto.oldPassword,
      user.password,
    );
    if (!match) {
      throw new BadRequestException('Wrong password');
    }

    await this.updateNakamaToken(user);
    return await this.usersService.updatePassword(
      user.id,
      changePasswordDto.newPassword,
    );
  }

  async login(user: User) {
    const refreshPayload = {
      sub: user.id,
      exp:
        Math.round(new Date().getTime() / 1000) +
        this.configService.get<number>('refreshJwtDuration'),
    };
    const refreshToken = this.jwtService.sign(refreshPayload, {
      secret: this.configService.get('refreshJwtSecret'),
    });
    this.refreshTokensCacheService.add(
      user.id,
      user,
      refreshToken,
      refreshPayload.exp,
      SupportedServices.SSO,
    );
    const payload = {
      username: user.username,
      sub: user.id,
      exp:
        Math.round(new Date().getTime() / 1000) +
        this.configService.get<number>('sso.jwtDuration'),
    };
    const accessToken = this.jwtService.sign(payload, {
      secret: this.configService.get('sso.jwtSecret'),
    });
    return {
      refreshToken,
      accessToken,
    };
  }

  async refresh(user: User) {
    const payload = {
      username: user.username,
      sub: user.id,
      exp:
        Math.round(new Date().getTime() / 1000) +
        this.configService.get<number>('sso.jwtDuration'),
    };
    const accessToken = this.jwtService.sign(payload, {
      secret: this.configService.get('sso.jwtSecret'),
    });
    return {
      accessToken,
    };
  }

  async loginNakama(user: User) {
    if (!user.nakamaCustomId) {
      user = await this.usersService.update(user.id, {
        nakamaCustomId: this.genNakamaCustomCode(),
      });
    }
    const payload = {
      sub: user.id,
      exp:
        Math.round(new Date().getTime() / 1000) +
        this.configService.get<number>('refreshJwtDuration'),
    };
    const refreshToken = this.jwtService.sign(payload, {
      secret: this.configService.get('refreshJwtSecret'),
    });
    this.refreshTokensCacheService.add(
      user.id,
      user,
      refreshToken,
      payload.exp,
      SupportedServices.Nakama,
    );
    // authenticate to nakama using a custom rpc
    try {
      const response = await lastValueFrom(
        this.httpService.post(
          this.configService.get('nakama.authEndpoint'),
          {
            username: user.username,
            customId: user.nakamaCustomId,
            expires:
              Math.round(new Date().getTime() / 1000) +
              this.configService.get<number>('nakama.jwtDuration'),
          },
          {
            params: {
              http_key: this.configService.get('nakama.httpSecret'),
              unwrap: '',
            },
          },
        ),
      );
      return {
        accessToken: response.data.result.token,
        refreshToken,
      };
    } catch (err) {
      this.logger.error(
        `Error authenticating to nakama: ${err.response.data.error.Object}, code: ${err.response.data.code}`,
      );
      throw new InternalServerErrorException();
    }
  }

  async refreshNakama(user: User) {
    try {
      const response = await lastValueFrom(
        this.httpService.post(
          this.configService.get('nakama.authEndpoint'),
          {
            username: user.username,
            customId: user.nakamaCustomId,
            expires:
              new Date().getTime() +
              this.configService.get('nakama.jwtDuration'),
          },
          {
            params: {
              http_key: this.configService.get('nakama.httpSecret'),
              unwrap: '',
            },
          },
        ),
      );
      return {
        accessToken: response.data.result.token,
      };
    } catch (err) {
      this.logger.error(
        `Error authenticating to nakama: ${err.response.data.error.Object}, code: ${err.response.data.code}`,
      );
      throw new InternalServerErrorException();
    }
  }

  async updateNakamaToken(user: User) {
    try {
      const newNakamaCustomId = this.genNakamaCustomCode();
      const response = await lastValueFrom(
        this.httpService.post(
          this.configService.get('nakama.updateIdEndpoint'),
          {
            username: user.username,
            oldCustomId: user.nakamaCustomId,
            newCustomId: newNakamaCustomId,
          },
          {
            params: {
              http_key: this.configService.get('nakama.httpSecret'),
              unwrap: '',
            },
          },
        ),
      );
      return this.usersService.update(user.id, {
        nakamaCustomId: newNakamaCustomId,
      });
    } catch (err) {
      this.logger.error(
        `Error changing nakama custom id: ${err.response.data.error.Object}`,
      );
      throw new InternalServerErrorException();
    }
  }

  genNakamaCustomCode(): string {
    return uuidv4();
  }
}
