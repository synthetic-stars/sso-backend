import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  InternalServerErrorException,
  Patch,
  Post,
  Query,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiBearerAuth, ApiNotFoundResponse, ApiTags, ApiUnauthorizedResponse } from '@nestjs/swagger';
import { SupportedServices } from 'src/common/enum/supported-services.enum';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';

import { AuthService } from './auth.service';
import { ChangePasswordDto } from './dto/change-password.dto';
import { LoginQueryDto } from './dto/login-query.dto';
import { LoginResponseDto } from './dto/login-response.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { RefreshQueryDto } from './dto/refresh-query.dto';
import { RefreshResponseDto } from './dto/refresh-response.dto';
import { RegisterQueryDto } from './dto/register-query.dto';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { JwtAuthGuard, RefreshJwtAuthGuard } from './guards/jwt-auth.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService,
    private readonly configService: ConfigService,
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Post('register')
  async register(
    @Body() createUserDto: CreateUserDto,
    @Query() registerQeuryDto: RegisterQueryDto,
  ) {
    const [user, backupCodes] = await this.usersService.create(createUserDto);

    try {
      if (registerQeuryDto.service === SupportedServices.Nakama) {
        const response = await this.authService.loginNakama(user);
        return { ...response, backupCodes };
      } else if (registerQeuryDto.service === SupportedServices.SSO) {
        const response = await this.authService.login(user);
        return { ...response, backupCodes };
      }
    } catch (err) {
      await this.usersService.remove(user.id);
      throw new InternalServerErrorException();
    }
  }

  @ApiNotFoundResponse()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Request() req,
    @Body() loginUserDto: LoginUserDto,
    @Query() loginQueryDto: LoginQueryDto,
  ): Promise<LoginResponseDto> {
    if (loginQueryDto.service === SupportedServices.SSO) {
      return this.authService.login(req.user);
    } else if (loginQueryDto.service === SupportedServices.Nakama) {
      return this.authService.loginNakama(req.user);
    }
  }

  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(RefreshJwtAuthGuard)
  @Get('refresh')
  async refresh(
    @Request() req,
    @Query() refreshQueryDto: RefreshQueryDto,
  ): Promise<RefreshResponseDto> {
    if (refreshQueryDto.service === SupportedServices.SSO) {
      return this.authService.refresh(req.user);
    } else if (refreshQueryDto.service === SupportedServices.Nakama) {
      return this.authService.refreshNakama(req.user);
    }
  }

  @ApiUnauthorizedResponse()
  @UseInterceptors(ClassSerializerInterceptor)
  @Patch('resetpassword')
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return this.authService.resetPassword(resetPasswordDto);
  }

  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Patch('changepassword')
  async changePassword(
    @Request() req,
    @Body() changePasswordDto: ChangePasswordDto,
  ) {
    return this.authService.changePassword(req.user.id, changePasswordDto);
  }

  @ApiBearerAuth()
  @ApiUnauthorizedResponse()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Delete('deleteaccount')
  async deleteAccount(@Request() req) {
    return this.usersService.remove(req.user.id);
  }
}
