import { IsString } from 'class-validator';
import { Match } from 'src/common/decorators/match.decorator';

export class ChangePasswordDto {
  @IsString()
  oldPassword: string;
  @IsString()
  newPassword: string;
  @IsString()
  @Match('newPassword')
  confirmNewPassword: string;
}
