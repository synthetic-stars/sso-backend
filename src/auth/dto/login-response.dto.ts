import { IsString } from "class-validator";

export class LoginResponseDto {
  @IsString()
  refreshToken: string;
  @IsString()
  accessToken: string;
}
