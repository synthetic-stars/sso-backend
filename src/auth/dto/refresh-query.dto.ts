import { LoginQueryDto } from './login-query.dto';

export class RefreshQueryDto extends LoginQueryDto {}
