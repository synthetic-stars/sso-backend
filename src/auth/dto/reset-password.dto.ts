import { IsString } from 'class-validator';
import { Match } from 'src/common/decorators/match.decorator';

export class ResetPasswordDto {
  @IsString()
  readonly username: string;

  @IsString()
  readonly bakCode: string;

  @IsString()
  readonly password: string;

  @IsString()
  @Match('password')
  readonly confirmPassword: string;
}
