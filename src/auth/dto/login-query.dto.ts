import { IsEnum } from 'class-validator';
import { SupportedServices } from 'src/common/enum/supported-services.enum';

export class LoginQueryDto {
  @IsEnum(SupportedServices)
  service: SupportedServices;
}
