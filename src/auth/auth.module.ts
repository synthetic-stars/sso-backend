import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UsersModule } from 'src/users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtStrategy, RefreshJwtStrategy } from './strategies/jwt.strategy';
import { HttpModule } from '@nestjs/axios';
import { RefreshTokensCacheModule } from 'src/refresh-tokens-cache/refresh-tokens-cache.module';
import { RefreshTokensCacheService } from 'src/refresh-tokens-cache/refresh-tokens-cache.service';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({}),
    ConfigModule,
    HttpModule,
    RefreshTokensCacheModule,
  ],
  providers: [AuthService, LocalStrategy, RefreshJwtStrategy, JwtStrategy, ConfigService, RefreshTokensCacheService],
  controllers: [AuthController],
})
export class AuthModule {}
