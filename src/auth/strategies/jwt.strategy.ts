import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { RefreshTokensCacheService } from 'src/refresh-tokens-cache/refresh-tokens-cache.service';
import { UsersService } from 'src/users/users.service';
import { AuthService } from '../auth.service';

@Injectable()
export class RefreshJwtStrategy extends PassportStrategy(
  Strategy,
  'refreshJwt',
) {
  constructor(
    private readonly configService: ConfigService,
    private readonly authService: AuthService,
    private readonly refreshTokensCacheService: RefreshTokensCacheService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('refreshJwtSecret'),
      passReqToCallback: true,
    });
  }

  async validate(request: Request, payload: any) {
    const jwt = request.headers.authorization.split(' ')[1];
    const service = request.query.service.toString();
    if (
      !(await this.refreshTokensCacheService.isValid(payload.sub, jwt, service))
    ) {
      return new UnauthorizedException();
    }

    return this.refreshTokensCacheService.getUser(payload.sub);
  }
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'accessJwt') {
  constructor(
    private readonly configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('sso.jwtSecret'),
    });
  }

  async validate(payload: any) {
    return { id: payload.sub, username: payload.username };
  }
}
