import { IsEnum, IsNumber, IsPositive, IsString } from 'class-validator';
import { SupportedServices } from 'src/common/enum/supported-services.enum';
import { User } from 'src/users/entities/user.entity';

class RefreshTokenBodyDto {
  @IsString()
  token: string;

  @IsNumber()
  @IsPositive()
  exp: number;

  @IsEnum(SupportedServices)
  service: SupportedServices;
}

export class RefreshTokenCacheDto {
  @IsNumber()
  @IsPositive()
  userId: number;

  tokens: RefreshTokenBodyDto[];

  user: User;
}
