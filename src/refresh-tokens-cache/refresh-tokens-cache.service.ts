import { Injectable } from '@nestjs/common';
import { SupportedServices } from 'src/common/enum/supported-services.enum';
import { User } from 'src/users/entities/user.entity';
import { RefreshTokenCacheDto } from './dto/refresh-tokens.dto';

@Injectable()
export class RefreshTokensCacheService {
  private refreshTokensCache: RefreshTokenCacheDto[] = [];

  async add(
    userId: number,
    user: User,
    token: string,
    exp: number,
    service: SupportedServices,
  ) {
    if (!this.refreshTokensCache.find((item) => item.userId === userId)) {
      this.refreshTokensCache.push({
        userId: userId,
        user: user,
        tokens: [
          {
            token,
            exp,
            service,
          },
        ],
      });
    } else {
      const idx = this.refreshTokensCache.findIndex(
        (item) => item.userId === userId,
      );
      this.refreshTokensCache[idx].tokens.push({
        token,
        exp,
        service,
      });
    }
  }

  async isValid(userId: number, token: string, service: string): Promise<boolean> {
    const idx = this.refreshTokensCache.findIndex((item) => item.userId === userId);
    if (idx < 0) {
      return false;
    }
    const match = this.refreshTokensCache[idx].tokens.findIndex(
      (item) => item.token === token && item.service === service,
    );
    if (match < 0) {
      return false;
    }

    return true;
  }

  getUser(userId: number): User {
    const idx = this.refreshTokensCache.findIndex((item) => item.userId === userId);
    if (idx < 0) {
      return undefined;
    }
    
    return this.refreshTokensCache[idx].user;
  }
}
