import { Test, TestingModule } from '@nestjs/testing';
import { RefreshTokensCacheService } from './refresh-tokens-cache.service';

describe('RefreshTokensCacheService', () => {
  let service: RefreshTokensCacheService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RefreshTokensCacheService],
    }).compile();

    service = module.get<RefreshTokensCacheService>(RefreshTokensCacheService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
