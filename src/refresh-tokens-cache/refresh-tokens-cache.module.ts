import { Module } from '@nestjs/common';
import { RefreshTokensCacheService } from './refresh-tokens-cache.service';

@Module({
  providers: [RefreshTokensCacheService]
})
export class RefreshTokensCacheModule {}
