import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from './auth/auth.module';
import appConfig from './config/app.config';
import { UsersModule } from './users/users.module';

import { validate } from './config/env.validation';
import { RefreshTokensCacheModule } from './refresh-tokens-cache/refresh-tokens-cache.module';

@Module({
  imports: [
    AuthModule,
    ConfigModule.forRoot({ load: [appConfig], validate }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'postgres',
        host: configService.get('database.host'),
        port: +configService.get<number>('database.port'),
        username: configService.get('database.username'),
        password: configService.get('database.password'),
        database: configService.get('database.name'),
        entities: ['dist/**/*.entity{.ts,.js}'],
        synchronize: configService.get('environment') === 'development',
      }),
      inject: [ConfigService],
    }),
    UsersModule,
    RefreshTokensCacheModule,
  ],
})
export class AppModule {}
