export default () => ({
  environment: process.env.NODE_ENV || 'development',
  refreshJwtSecret: process.env.REFRESH_JWT_SECRET,
  refreshJwtDuration: parseInt(process.env.REFRESH_JWT_DURATION),
  database: {
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    name: process.env.DATABASE_NAME,
  },
  sso: {
    jwtSecret: process.env.SSO_JWT_SECRET,
    jwtDuration: parseInt(process.env.SSO_JWT_DURATION),
  },
  nakama: {
    jwtDuration: parseInt(process.env.NAKAMA_JWT_DURATION),
    authEndpoint: process.env.NAKAMA_AUTH_ENDPOINT,
    httpSecret: process.env.NAKAMA_HTTP_SECRET,
    updateIdEndpoint: process.env.NAKAMA_UPDATE_ID_ENDPOINT
  },
  bcryptSaltRounds: parseInt(process.env.BCRYPT_SALT_ROUNDS) || 8,
});
