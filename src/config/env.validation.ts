import { plainToClass } from 'class-transformer';
import {
  IsEnum,
  IsNumber,
  IsPositive,
  IsString,
  IsUrl,
  validateSync,
} from 'class-validator';

enum Environment {
  Development = 'development',
  Production = 'production',
  Test = 'test',
  Provision = 'provision',
}

class EnvironmentVariables {
  @IsEnum(Environment)
  NODE_ENV: Environment;

  @IsString()
  DATABASE_HOST: string;

  @IsNumber()
  @IsPositive()
  DATABASE_PORT: number;

  @IsString()
  DATABASE_USER: string;

  @IsString()
  DATABASE_PASSWORD: string;

  @IsString()
  DATABASE_NAME: string;

  @IsString()
  REFRESH_JWT_SECRET: string;

  @IsNumber()
  @IsPositive()
  REFRESH_JWT_DURATION: number;

  @IsString()
  SSO_JWT_SECRET: string;

  @IsNumber()
  @IsPositive()
  SSO_JWT_DURATION: number;

  @IsNumber()
  @IsPositive()
  NAKAMA_JWT_DURATION: number;

  @IsUrl()
  NAKAMA_AUTH_ENDPOINT: string;

  @IsUrl()
  NAKAMA_UPDATE_ID_ENDPOINT: string;

  @IsString()
  NAKAMA_HTTP_SECRET: string;

  @IsNumber()
  @IsPositive()
  BCRYPT_SALT_ROUNDS: number;
}

export function validate(config: Record<string, unknown>) {
  const validatedConfig = plainToClass(EnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
}
